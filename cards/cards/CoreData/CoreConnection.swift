//
//  CoreConnection.swift
//  cards
//
//  Created by Carlos Velasquez on 11/08/22.
//

import Foundation

public class CoreConnection {
    private let dataPath = "appdata"
//    private var theChapters;
    
    private func readLocalDataFile(forName name: String) -> Data? {
        do {
            if let bundlePath = Bundle.main.path(forResource: name, ofType: "json"),
                let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
                    return jsonData
            }
        } catch {
            print(error)
        }
        
        return nil
    }
    
    private func parseDataWords(jsonData: Data) -> Words {
        do {
            let decodedData = try JSONDecoder().decode(ResponseDataWords.self,
                                                       from: jsonData)
            
            return decodedData.words
        } catch {
            print("decode error", error)
        }
        
        return []
    }
    
    
    func getWords(completion : @escaping (Words) -> ())  {
        if let localData = self.readLocalDataFile(forName: dataPath) {
            let dataWords = self.parseDataWords(jsonData: localData)
            
            completion(dataWords)
        } else {
            completion([])
        }
    }
}
