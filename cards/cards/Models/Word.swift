//
//  Words.swift
//  cards
//
//  Created by Carlos Velasquez on 11/08/22.
//

import Foundation

struct ResponseDataWords: Decodable {
    var words: Words
}

typealias Words = [Word]
typealias Options = [Option]

// MARK: - PostInfo
struct Word: Decodable {
    let id: Int
    let principal: String, options: Options

    enum CodingKeys: String, CodingKey {
        case id, principal, options
    }
}

struct Option: Decodable {
    let id: Int, value: Int
    let word: String

    enum CodingKeys: String, CodingKey {
        case id, word, value
    }
}
