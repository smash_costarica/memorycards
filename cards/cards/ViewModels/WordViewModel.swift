//
//  WordViewModel.swift
//  cards
//
//  Created by Carlos Velasquez on 11/08/22.
//

import Foundation

class WordViewModel: NSObject {
    private var coreService : CoreConnection!
    private(set) var words : Words! {
        didSet {
            self.bindWordsViewModelToController()
        }
    }
    
    var bindWordsViewModelToController : (() -> ()) = {}
    
    override init() {
        super.init()
        self.coreService =  CoreConnection()
        self.callFuncToGetWords()
    }
    
    func callFuncToGetWords() {
        self.coreService.getWords  { datas in
            self.words = datas
        }
    }
    
    func callFuncToRandomWord() {
        self.callFuncToGetWords()
    }
}
