//
//  HomeViewController.swift
//  cards
//
//  Created by Carlos Velasquez on 11/08/22.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak var lblWordSelected: UILabel!
    @IBOutlet weak var collectionWordView: UICollectionView!
    let reuseIdentifier = "OptionWordsCell"
    private var wordViewModel : WordViewModel!
    private var indexSelectedCard: Int?
    
    //MARK: - override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.wordViewModel = WordViewModel()
        // Do any additional setup after loading the view.
        initGame()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        collectionWordView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK: - Functionality
    func initGame() {
        self.indexSelectedCard = Int(arc4random_uniform(UInt32(self.wordViewModel.words.count)))
        setDataWordSelected()
        collectionWordView.reloadData()
    }
    
    func setDataWordSelected() {
        lblWordSelected.text = self.wordViewModel.words[self.indexSelectedCard!].principal
    }
    
    func verifyWin(selectedItem: Int) -> String {
        if self.wordViewModel.words[self.indexSelectedCard!].options[selectedItem].value == 1 {
            return "Good"
        }
        
        if self.wordViewModel.words[self.indexSelectedCard!].options[selectedItem].value == 2 {
            return "So so"
        }
        
        if self.wordViewModel.words[self.indexSelectedCard!].options[selectedItem].value == 3 {
            return "Bad"
        }
        
        return ""
    }
    
    func showAlertWinner(itemSelected: Int) {
        
        let textResult = verifyWin(selectedItem: itemSelected)
        
        // create the alert
        let alert = UIAlertController(title: "Result", message: "Your selection is: \(textResult)", preferredStyle: UIAlertController.Style.alert)

        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { UIAlertAction in
            self.initGame()
        }))

        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Actions buttons
    @IBAction func touchUpInsideNext(_ sender: Any) {
        initGame()
    }

}

extension HomeViewController: UICollectionViewDataSource {
//  func photo(for indexPath: IndexPath) -> Word {
////    return searches[indexPath.section].searchResults[indexPath.row]
//  }
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1     //return number of sections in collection view
    }
    
    // MARK: - UICollectionViewDataSource protocol
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.wordViewModel.words[indexSelectedCard!].options.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! WordCollectionViewCell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        cell.lblDesc.text = self.wordViewModel.words[indexSelectedCard!].options[indexPath.row].word // The row value is the same as the index of the desired text within the array.
        cell.contentView.backgroundColor = UIColor.lightGray // make cell more visible in our example project
        
        return cell
    }
}


extension HomeViewController: UICollectionViewDelegate {
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        if let cell = collectionView.cellForItem(at: indexPath){
            cell.contentView.backgroundColor = .cyan
        }

        showAlertWinner(itemSelected: indexPath.item)
    }
}
